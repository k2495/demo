# Demo

The purpose of this project is to demonstrate how to start a Kafka and Flink stack that involve a custom producer and a simple stream process.

## Getting started

Disclaimer ! You may switch `podman` with `docker` in the following commands, depending on your setup.

⚠️ Make sure your container runtime can login to `registry.gitlab.com`:

```shell
podman login -u <login> --password <access_token> registry.gitlab.com
```

Note: be sure the `access_token` you are using enable the `read_registry` scope.

Once authentication is settled, start the images:

```shell
podman-compose pull
````

Then, start the stack:

```shell
podman-compose up -d
```

After a while, you should be able to access Flink Jobmanager dashboard at [localhost:8081](http://localhost:8081/#/overview).
Jump to "Flink Streaming Job" by clicking on it in the "Running Jobs" list.

You should see something like this:

![ Flink Streaming Job Overview](img/flink-stream-job_overview.png)

Once you're down with this demo, shutdown the stack:

```shell
podman-compose down -v
```

## Behind the scene

This stack is fairly simple: there is an event producer [`kafka-producer`](https://gitlab.com/k2495/kafka-producer), Kafka and an event event 
consumer [`flink-consumer`](https://gitlab.com/k2495/flink-consumer), thx captain obvious... 🥸

Feel free to have a look for better understanding of how it works. Simply puts, events representing operations on user contact informations
 are emitted into Kafka in order to be rationalized in Flink to finally feed operation kind dedicated Kafka topics.

The producer is spring boot application, and the consumer is a Flink distribution running in 
[Application Mode](https://nightlies.apache.org/flink/flink-docs-release-1.14/docs/deployment/resource-providers/standalone/docker/#application-mode).

The latter is configured to run only the demo Job, check the [`services.jobmanager.command`](https://gitlab.com/k2495/demo/-/blob/master/docker-compose.yml#L41) 
in `docker-compose.yml`.
